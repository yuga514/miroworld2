#include "OBJShaderHeader.hlsli"

VSOutput main(float4 pos : POSITION, float3 normal : NORMAL, float2 uv : TEXCOORD)
{
	float4 wpos = mul(world, pos);
	float4 wnormal = normalize(mul(world, float4(normal, 0)));

	//ピクセルシェーダーに渡す値
	VSOutput output;
	output.svpos = mul(mul(viewproj, world), pos);
	output.worldpos = wpos;
	output.uv = uv;
	output.normal = wnormal;
	return output;
}