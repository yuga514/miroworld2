#include "Light.h"

using namespace DirectX;

ID3D12Device* Light::device = nullptr;

Light* Light::Create()
{
	Light* instance = new Light();

	instance->Initalize();

	return instance;
}
void Light::StaticInitalize(ID3D12Device* device)
{
	assert(!Light::device);

	assert(device);

	Light::device = device;
}
void Light::Initalize()
{
	HRESULT result;

	DefaultLightSetting();

	CD3DX12_HEAP_PROPERTIES properties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
	CD3DX12_RESOURCE_DESC desc = CD3DX12_RESOURCE_DESC::Buffer((sizeof(ConstBufferData) + 0xff) & ~0xff);
	result = device->CreateCommittedResource(
		&properties,
		D3D12_HEAP_FLAG_NONE,
		&desc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&constBuff)
	);

	if (FAILED(result)) { assert(0); }

	TarnsferConstBuffer();
}
void Light::Update()
{
	//変更があればバッファ転送
	if (dirty) {
		TarnsferConstBuffer();
		dirty = false;
	}
}
void Light::Draw(ID3D12GraphicsCommandList* cmdList, UINT rootParameterIndex)
{
	cmdList->SetGraphicsRootConstantBufferView(
		rootParameterIndex,
		constBuff->GetGPUVirtualAddress());
}
void Light::TarnsferConstBuffer()
{
	HRESULT result;

	ConstBufferData* constMap = nullptr;
	result = constBuff->Map(0, nullptr, (void**)&constMap);
	if (SUCCEEDED(result)) {
		constMap->color = lightcolor;

		for (int i = 0; i < DirLightNum; i++)
		{
			if (dirLights[i].IsActive()) {
				constMap->dirRights[i].active = 1;
				constMap->dirRights[i].lightv = -dirLights[i].GetLightDir();
				constMap->dirRights[i].lightcolor = dirLights[i].GetLightColor();
			}
			else { 
				constMap->dirRights[i].active = 0; 
			}
		}
		// 点光源
		//for (int i = 0; i < PointLightNum; i++) {
		//	// ライトが有効なら設定を転送
		//	if (pointLights[i].IsActive()) {
		//		constMap->pointLights[i].active = 1;
		//		constMap->pointLights[i].lightpos = pointLights[i].GetLightPos();
		//		constMap->pointLights[i].lightcolor = pointLights[i].GetLightColor();
		//		constMap->pointLights[i].lightatten = pointLights[i].GetLightAtten();
		//	}
		//	// ライトが無効ならライト色を0に
		//	else {
		//		constMap->pointLights[i].active = 0;
		//	}
		//}
		//for (int i = 0; i < CircleShadowNum; i++)
		//{
		//	if (circleShadows[i].IsActive()) {
		//		constMap->circleShadows[i].active = 1;
		//		constMap->circleShadows[i].dir = -circleShadows[i].GetDir();
		//		constMap->circleShadows[i].casterPos = circleShadows[i].GetCasterPos();
		//		constMap->circleShadows[i].distanceCasterLight = circleShadows[i].GetDistanceCasterPos();
		//		constMap->circleShadows[i].atten = circleShadows[i].GetAtten();
		//		constMap->circleShadows[i].factorAngleCos = circleShadows[i].GetFactorAngleCos();
		//	}
		//	else{
		//		constMap->circleShadows[i].active = 0;
		//	}
		//}
		constBuff->Unmap(0, nullptr);
	}
}
void Light::DefaultLightSetting()
{
	dirLights[0].SetActive(true);
	dirLights[0].SetLightColor({ 1.0f, 1.0f, 1.0f });
	dirLights[0].SetLightDir({ 0.0f, -1.0f, 0.0f, 0 });

	dirLights[1].SetActive(true);
	dirLights[1].SetLightColor({ 1.0f, 1.0f, 1.0f });
	dirLights[1].SetLightDir({ +0.5f, +0.1f, +0.2f, 0 });

	dirLights[2].SetActive(true);
	dirLights[2].SetLightColor({ 1.0f, 1.0f, 1.0f });
	dirLights[2].SetLightDir({ -0.5f, +0.1f, -0.2f, 0 });
}

void Light::SetLightColor(const XMFLOAT3& lightcolor)
{
	this->lightcolor = lightcolor;
	dirty = true;
}

//void Light::SetCircleShadowActive(int index, bool active)
//{
//	assert(0 <= index && index < CircleShadowNum);
//	circleShadows[index].SetActive(active);
//	dirty = true;
//}
//void Light::SetCircleShadowCasterPos(int index, const XMFLOAT3& casterPos)
//{
//	assert(0 <= index && index < CircleShadowNum);
//	circleShadows[index].SetCasterPos(casterPos);
//	dirty = true;
//}
//void Light::SetCircleShadowDir(int index, const XMVECTOR& lightDir)
//{
//	assert(0 <= index && index < CircleShadowNum);
//	circleShadows[index].SetDir(lightDir);
//	dirty = true;
//}
//void Light::SetCircleShadowDistanceCasterLight(int index, float distanceCasterLight)
//{
//	assert(0 <= index && index < CircleShadowNum);
//	circleShadows[index].SetDistanceCasterPos(distanceCasterLight);
//	dirty = true;
//}
//void Light::SetCircleShadowAtten(int index, const XMFLOAT3& lightAtten)
//{
//	assert(0 <= index && index < CircleShadowNum);
//	circleShadows[index].SetAtten(lightAtten);
//	dirty = true;
//}
//void Light::SetCircleShadowFacterAngle(int index, const XMFLOAT2& factorAngle)
//{
//	assert(0 <= index && index < CircleShadowNum);
//	circleShadows[index].SetFactorAngle(factorAngle);
//	dirty = true;
//}

//void Light::SetPointLightActive(int index, bool active)
//{
//	assert(0 <= index && index < PointLightNum);
//
//	pointLights[index].SetActive(active);
//	dirty = true;
//}
//void Light::SetPointLightPos(int index, const XMFLOAT3& lightpos)
//{
//	assert(0 <= index && index < PointLightNum);
//
//	pointLights[index].SetLightPos(lightpos);
//	dirty = true;
//}
//void Light::SetPointLightColor(int index, const XMFLOAT3& lightcolor)
//{
//	assert(0 <= index && index < PointLightNum);
//
//	pointLights[index].SetLightColor(lightcolor);
//	dirty = true;
//}
//void Light::SetPointLightAtten(int index, const XMFLOAT3& lightAtten)
//{
//	assert(0 <= index && index < PointLightNum);
//
//	pointLights[index].SetLightAtten(lightAtten);
//	dirty = true;
//}

void Light::SetDirLightActive(int index, bool active)
{
	assert(0 <= index && index < DirLightNum);

	dirLights[index].SetActive(active);
	dirty = true;
}
void Light::SetDirLightDir(int index, const XMVECTOR& lightdir)
{
	assert(0 <= index && index < DirLightNum);

	dirLights[index].SetLightDir(lightdir);
	dirty = true;
}
void Light::SetDirLightColor(int index, const XMFLOAT3& lightcolor)
{
	assert(0 <= index && index < DirLightNum);

	dirLights[index].SetLightColor(lightcolor);
	dirty = true;
}

