#include "Floor.h"
#include "BaseCollider.h"
#include "CollisionManager.h"
#include "Easing.h"
#include <fstream>

Floor::~Floor()
{
}

bool Floor::Initialize(XMFLOAT3 pos,XMFLOAT2 point)
{
	object = Object3d::Create();
	object->SetModel(Model::CreateFromOBJ("FloorBox"));
	object->SetScale({ 1.5f, 1.5f, 1.5f });
	shake.reset(new Shake());
	position = pos;
	obb.Create();
	//点線のブロック(仮)ーー後からcsvで読み込ませるようにする
	return true;
}

void Floor::Update()
{
	object->SetPosition(position);

	obb.SetVector(0, XMVECTOR{ 1, 0, 0, 0 });
	obb.SetVector(1, XMVECTOR{ 0, 1, 0, 0 });
	obb.SetVector(2, XMVECTOR{ 0, 0, 1, 0 });
	obb.SetPos(position);
	obb.SetLength(0, 1.46f);
	obb.SetLength(1, 3.f);
	obb.SetLength(2, 1.46f);
	object->Update();
}

void Floor::Draw()
{
	object->Draw();
}