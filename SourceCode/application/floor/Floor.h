#pragma once
#include "GameObject.h"
#include "Fbx_Object3d.h"
#include "DirectXCommon.h"
#include "Object3d.h"
#include "TouchableObject.h"
#include "Shake.h"
#include "Audio.h"
#include "OBB.h"
#include <DirectXMath.h>
#include <memory>

using namespace DirectX;

class Floor : public Object3d
{
public:
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;

public:
	~Floor();

	bool Initialize(XMFLOAT3 pos,XMFLOAT2 point);

	void Update();

	void Draw();

public:
	inline OBB GetObb() { return obb; }
	inline XMFLOAT3 GetPos() { return position; }

protected:
	XMFLOAT3 position;
	std::unique_ptr<Object3d> object;
	bool alive = true;
	XMFLOAT4 color = {1.0f, 1.0f, 1.0f, 1.0f};
	//イージング用
	float easFrame = 0.0f;
	float downFrame = 0.0f;
	//シェイク用
	std::unique_ptr<Shake> shake = nullptr;
	bool shakeF = false;
	XMFLOAT3 shakePos = { 0.0f,0.0f,0.0f };
	Audio* audio = nullptr;
	OBB obb = {};
};