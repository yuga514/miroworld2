#pragma once
#include "GameObject.h"
#include "Fbx_Object3d.h"
#include "DirectXCommon.h"
#include "Object3d.h"
#include "Shake.h"
#include "Audio.h"
#include "OBB.h"
#include <DirectXMath.h>
#include <memory>

using namespace DirectX;

class CFloor : public Object3d
{
public:
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMVECTOR = DirectX::XMVECTOR;
	using XMFLOAT4 = DirectX::XMFLOAT4;

public:
	~CFloor();

	bool Initialize(XMFLOAT3 pos,XMFLOAT2 point);

	void Update();

	void OnCollision(bool flag = false);
	inline OBB GetObb() { return obb; }
protected:
	//XMFLOAT3 position;
	std::unique_ptr<Object3d> object;
	bool alive = true;
	XMFLOAT4 color = { 1.0f, 1.0f, 1.0f, 1.0f };
	//イージング用
	float easFrame = 0.0f;
	float downFrame = 0.0f;
	//シェイク用
	std::unique_ptr<Shake> shake = nullptr;
	bool shakeF = false;
	XMFLOAT3 shakePos = { 0.0f,0.0f,0.0f };
	Audio* audio = nullptr;
	OBB obb = {};
};