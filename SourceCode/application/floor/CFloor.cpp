#include "CFloor.h"

CFloor::~CFloor()
{
}

bool CFloor::Initialize(XMFLOAT3 pos,XMFLOAT2 point)
{
	object = Object3d::Create();
	object->SetModel(Model::CreateFromOBJ("FloorBox"));
	object->SetScale({ 1.5f, 1.5f, 1.5f });
	shake.reset(new Shake());
	position = pos;
	obb.Create();

	return true;
}

void CFloor::Update()
{
	object->SetPosition(position);
	obb.SetVector(0, XMVECTOR{ 1, 0, 0, 0 });
	obb.SetVector(1, XMVECTOR{ 0, 1, 0, 0 });
	obb.SetVector(2, XMVECTOR{ 0, 0, 1, 0 });
	obb.SetPos(position);
	obb.SetLength(0, 1.46f);
	obb.SetLength(1, 1.46f);
	obb.SetLength(2, 1.46f);
	object->Update();

}

void CFloor::OnCollision(bool flag)
{
}
