#pragma once
#include "BaseScene.h"
#include "Object3d.h"
#include "Sprite.h"
#include "DebugCamera.h"
#include "DirectXCommon.h"
#include "Fbx_Object3d.h"
#include "Input.h"
#include "TouchableObject.h"
#include "Camera.h"
#include "FollowingCamera.h"
#include "DebugText.h"
#include "Player.h"
#include "PlayerRight.h"
#include "Framework.h"
#include "Material.h"
#include "PostEffect.h"
#include "Audio.h"
#include "Floor.h"
#include "Goal.h"
#include "ClearBox.h"
#include "BlueBox.h"
#include "RedBox.h"
#include "BlueCBox.h"
#include "RedCBox.h"
#include "CFloor.h"
#include "CollisionManager.h"
#include "Key.h"
#include "KeyHole.h"
#include "KeyHoleR.h"
#include "Warp.h"
#include <sstream>

class Stage12 : public BaseScene
{
public:
	/// <summary>
	/// 初期化
	/// </summary>
	void Initialize(DirectXCommon* dxCommon) override;

	/// <summary>
	/// 終了
	/// </summary>
	void Finalize() override;

	/// <summary>
	/// 毎フレーム更新
	/// </summary>
	void Update() override;

	/// <summary>
	/// 描画
	/// </summary>
	void Draw(DirectXCommon* dxCommon) override;

	//当たり判定
	void PlayerWarp();
	void PlayerRWarp();

	void CheckRedCollision();
	void CheckRedCCollision();

	void CheckBlueCollision();
	void CheckBlueCCollision();

	void BlueAndBlueCollision();
	void RedAndRedCollision();
	void BlueAndRedCCollision();
	void RedAndBlueCCollision();
	void CheckPlayerCollision();

	/// <summary>
	/// 障害物の発生データの読み込み
	/// </summary>
	void LoadPopData();

	/// <summary>
	/// 障害物配置のコマンド更新
	/// </summary>
	void UpdataPopCommand();

private:
	Audio* audio = nullptr;

	Input* input = nullptr;
	DebugText* debugText = nullptr;
	//スプライト
	std::unique_ptr<Sprite> clearSprite;
	std::unique_ptr<Sprite> moveUI;
	std::unique_ptr<Sprite> keySprite[3];
	// 数値の表示
	char str[100];
	//床
	std::list<std::unique_ptr<Floor>> floors;
	std::stringstream floorPopCom;
	//透明のBOX
	std::list<std::unique_ptr<ClearBox>> clearBox;
	std::list<std::unique_ptr<CFloor>> cFloor;
	std::list<std::unique_ptr<BlueBox>> blueBoxs;
	std::list<std::unique_ptr<RedBox>> redBoxs;
	std::list<std::unique_ptr<BlueCBox>> blueCBoxs;
	std::list<std::unique_ptr<RedCBox>> redCBoxs;
	//ゴール
	std::list<std::unique_ptr<Goal>> goals;
	CollisionManager* collisionMan = nullptr;
	std::unique_ptr<PostEffect> postEffect;
	bool startFlag = false;
	float startEfRadius = 0;
	//ゲームが終わった時に暗くするやつの半径
	float endEfRadius = 1000.f;
	bool backFlag = false;
	float backEfRadius = 1000.f;
	//天球
	std::unique_ptr<Object3d> skyObj;
	std::unique_ptr<Framework> frame;
	std::unique_ptr<FollowingCamera> camera;
	std::unique_ptr<DebugCamera> debugCam;
	Camera* nowCamera = nullptr;
	XMFLOAT3 cameraPos;
	float angle = 0.f;

	std::unique_ptr<Warp> warp;
	//パーティクル
	ParticleManager* particleMan = nullptr;
	std::unique_ptr<Material> material;
	float alpha = 0.f;
	//XMFLOAT3 cameraPos;
	std::unique_ptr<Player> player;
	std::unique_ptr<PlayerRight> playerRight;
	std::unique_ptr<Object3d> smallPlayer;
	XMFLOAT3 playerPos;
	XMFLOAT3 playerRPos;
	int32_t rotateTime = 120;
	bool rotateFlag = false;
	bool redonFlag = false;
	bool blueonFlag = false;
	bool playerOnfloor = false;
	bool blockToblock = false;
	bool goalFlag = false;
	bool endFlag = false;
	bool warpFlag = false;
	bool warpRFlag = false;
	bool scaleFlag = false;
	bool scaleRFlag = false;
	bool keyFlag = false;
	bool keyGetFlag = false;
	float keyTimer = 0.f;
	int keynum = 0;
	float keyFlagTime = 0.f;
	XMFLOAT3 cameraTargetPos;
	XMFLOAT3 playerRot;
	XMFLOAT3 playerRRot;
	//XMFLOAT3 playerPos;
	XMFLOAT3 playerScale;
	XMFLOAT3 playerRScale;
	float easFrame = 0.0f;
	// パーティクル出すフレーム数
	float ParticleFrame = 0.f;
	// カメラ関係
	bool RFlag = false;
	bool dirty = false;
	float angleX = 0;
	float angleY = 0;
	float scaleX = 0.35f / (float)WinApp::window_width;
	float scaleY = 0.35f / (float)WinApp::window_height;
	bool viewDirty = false;
	float distance = 1.0f;
	XMMATRIX matRot = DirectX::XMMatrixIdentity();

	std::unique_ptr<Sprite> black;
	bool sceneChangeFlag = false;
	XMFLOAT4 BlackColor = { 1,1,1,0 };
};

