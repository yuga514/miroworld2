#include "Stage5.h"
#include "Stage6.h"
#include "TitleScene.h"
#include "SceneManager.h"
#include "Audio.h"
#include "Input.h"
#include "DebugText.h"
#include "FbxLoader.h"
#include "Fbx_Object3d.h"
#include "GamePlayScene.h"
#include "Collision.h"
#include "SphereCollider.h"
#include "CollisionManager.h"
#include "MeshCollider.h"
#include "ParticleManager.h"
#include "Easing.h"
#include "OBB.h"
#include "OBBCollision.h"
#include <fstream>

void Stage5::Initialize(DirectXCommon* dxCommon)
{
	Audio* audio = Audio::GetInstance();
	audio->SoundStop("gameclear.wav");

	audio->SoundLoadWave("Mercury_Lamp.wav");
	audio->SoundLoadWave("gameclear.wav");
	//カメラの初期化
	camera.reset(new FollowingCamera());
	//カメラの初期化
	debugCam.reset(new DebugCamera(WinApp::window_width, WinApp::window_height));

	//スプライト読み込み
	Sprite::LoadTexture(1, L"Resources/StageClear.png");
	clearSprite.reset(Sprite::Create(1, { 640,360 }));
	//スプライト読み込み
	Sprite::LoadTexture(2, L"Resources/rule.png");
	moveUI.reset(Sprite::Create(2, { 640,360 }));
	// スプライト読み込み
	Sprite::LoadTexture(10, L"Resources/black.png");
	black.reset(Sprite::Create(10, { 640,360 }, BlackColor));
	//データ読み込み
	skyObj = Object3d::Create();
	skyObj->SetModel(Model::CreateFromOBJ("skydome"));

	//自機のオブジェクトセット+初期化
	player.reset(Player::Create(Model::CreateFromOBJ("RedBox")));
	player->SetPosition({ -39, 10.f, 0 });
	//カメラ追従用オブジェクト
	smallPlayer = Object3d::Create();
	smallPlayer->SetModel(Model::CreateFromOBJ("smallPlayer"));

	//自機のオブジェクトセット+初期化
	playerRight.reset(PlayerRight::Create(Model::CreateFromOBJ("BlueBox")));
	playerRight->SetPosition({ 39, 10.f, 0 });

	//パーティクのインスタンス
	particleMan = ParticleManager::GetInstance();
	particleMan->Initialize();
	particleMan->SetCamera(camera.get());
	//当たり判定のインスタンス
	collisionMan = CollisionManager::GetInstance();

	postEffect.reset(new PostEffect());
	postEffect->CreateGraphicsPipelineState(L"Resources/shaders/PostEffectPS.hlsl");
	//シェーダーの挿入
	postEffect->Initialize();

	//マップチップ読み込み用
	LoadPopData();
}

void Stage5::Finalize()
{
	player.reset();
	playerRight.reset();
	skyObj.reset();
	smallPlayer.reset();
	moveUI.reset();
	camera.reset();
	debugCam.reset();
	black.reset();
	clearSprite.reset();
	blueBoxs.clear();
	redBoxs.clear();
	blueCBoxs.clear();
	redCBoxs.clear();
	floors.clear();
	clearBox.clear();
	cFloor.clear();
}

void Stage5::Update()
{
	audio = Audio::GetInstance();
	audio->SoundPlayWave("Mercury_Lamp.wav", true);
	startEfRadius += 10.5f;
	if (startEfRadius >= 1000) {
		startEfRadius -= 10.5f;
		startFlag = true;
	}
	Input* input = Input::GetInstance();
	if (input->TriggerKey(DIK_R) && !goalFlag) {
		//シーン切り替え
		BaseScene* scene = new Stage5();
		this->sceneManager->SetNextScene(scene);
	}
	if (sceneChangeFlag) {
		BlackColor.w += 0.02f;
		black->SetColor(BlackColor);
	}
	if (1 < BlackColor.w) {
		// シーン切り替え
		BaseScene* scene = new Stage6();
		this->sceneManager->SetNextScene(scene);
	}

#ifdef _DEBUG
	if (input->TriggerKey(DIK_P)) {
		//シーン切り替え(リセット)
		BaseScene* scene = new Stage6();
		this->sceneManager->SetNextScene(scene);
	}
#endif

	//カメラを3Dオブジェットにセット
	nowCamera = camera.get();
	//カメラを3Dオブジェットにセット
	Object3d::SetCamera(nowCamera);

	camera->SetFollowingTarget(smallPlayer.get());

	//スペース押したらカメラ切り替え
	if (!player->GetMoveF() && !player->GetMoveF1() && !player->GetMoveF2() && !player->GetMoveF3()) {
		if (input->TriggerKey(DIK_SPACE) && !goalFlag) {
			if (!RFlag) {
				RFlag = true;
			}
			else {
				RFlag = false;
			}
		}
	}

	//プレイヤーがステージから落ちたらステージの真ん中に戻す
	if (player->GetPosition().y <= -20) {
		player->SetPosition({ -39,10,0 });
		player->SetRotation({ 0,0,0 });
		playerRight->SetPosition({ 39,10,0 });
		playerRight->SetRotation({ 0,0,0 });
	}
	//カメラ追従用オブジェクトのポジション
	if (!RFlag) {
		smallPlayer->SetPosition({ player->GetPosition().x,player->GetPosition().y,player->GetPosition().z });
		playerRight->SetPosition({ -player->GetPosition().x,player->GetPosition().y,player->GetPosition().z }); // 座標ずれ防止
		moveUI->SetIsFlipX(false); // UI反転
	}
	else {
		smallPlayer->SetPosition({ playerRight->GetPosition().x,playerRight->GetPosition().y,playerRight->GetPosition().z });
		player->SetPosition({ -playerRight->GetPosition().x,playerRight->GetPosition().y,playerRight->GetPosition().z }); // 座標ずれ防止
		moveUI->SetIsFlipX(true); // UI反転
	}

	//各オブジェクトの更新
	postEffect->SetRadius(startEfRadius);
	skyObj->Update();
	collisionMan->CheckAllCollisions();
	if (!goalFlag) {
		player->Update();
		playerRight->Update();
	}
	else {
		player->StopUpdate();
		playerRight->StopUpdate();
	}
	smallPlayer->Update();
	UpdataPopCommand();
	for (auto& floor : floors) {
		floor->Update();
	}
	for (auto& cBox : clearBox) {
		cBox->Update();
	}
	for (auto& cF : cFloor) {
		cF->Update();
	}
	for (auto& redBox : redBoxs) {
		redBox->Update();
	}
	for (auto& blueBox : blueBoxs) {
		blueBox->Update();
	}
	for (auto& redCBox : redCBoxs) {
		redCBox->Update();
	}
	for (auto& blueCBox : blueCBoxs) {
		blueCBox->Update();
	}
	for (auto& goal : goals) {
		goal->Update();
	}
	camera->Update();
	debugCam->Update();
	//当たり判定
	CheckRedCollision();
	CheckRedCCollision();
	CheckBlueCollision();
	CheckBlueCCollision();
	BlueAndRedCCollision();
	RedAndBlueCCollision();
	CheckPlayerCollision();
	BlueAndBlueCollision();
	RedAndRedCollision();

	particleMan->Update();
}

void Stage5::Draw(DirectXCommon* dxCommon)
{
	//描画前処理
	//dxCommon->PreDraw();
	//スプライト描画
#pragma region 背景スプライト描画
	postEffect->PreDrawScene(dxCommon->GetCmdList());

	// 背景スプライト描画前処理
	Sprite::PreDraw(dxCommon->GetCmdList());
	//背景スプライト描画

	/// <summary>
	/// ここに背景スプライトの描画処理を追加できる
	/// </summary>
	// スプライト描画後処理
	Sprite::PostDraw();
	// 深度バッファクリア
	dxCommon->ClearDepthBuffer(dxCommon->GetCmdList());
#pragma endregion

#pragma endregion

	//3Dオブジェクト描画前処理
	Object3d::PreDraw();
	skyObj->Draw();
	player->Draw();
	playerRight->Draw();
	for (auto& goal : goals) {
		goal->Draw();
	}
	for (auto& floor : floors) {
		floor->Draw();
	}
	for (auto& cBox : clearBox) {
		cBox->Draw();
	}
	for (auto& redBox : redBoxs) {
		redBox->Draw();
	}
	for (auto& redCBox : redCBoxs) {
		if (redCBox->GetHoolFlag()) {
			redCBox->Draw();
		}
	}
	for (auto& blueBox : blueBoxs) {
		blueBox->Draw();
	}
	for (auto& blueCBox : blueCBoxs) {
		if (blueCBox->GetHoolFlag()) {
			blueCBox->Draw();
		}
	}
	if (!rotateFlag) {
		particleMan->Draw();
	}
	Object3d::PostDraw();

	postEffect->PostDrawScene(dxCommon->GetCmdList());

	//描画前処理
	dxCommon->PreDraw();
	postEffect->Draw(dxCommon->GetCmdList());

#pragma region 前景スプライト描画
	// 前景スプライト描画前処理
	Sprite::PreDraw(dxCommon->GetCmdList());

	moveUI->Draw();
	if (goalFlag) {
		clearSprite->Draw();
	}
	black->Draw();
	// デバッグテキスト
	//DebugText::GetInstance()->Print("%d", 50, 50, 10);
	// スプライト描画後処理

	// デバッグテキストの描画
	DebugText::GetInstance()->DrawAll(dxCommon->GetCmdList());

	// スプライト描画後処理
	Sprite::PostDraw();

	//描画後処理
	dxCommon->PostDraw();
}

void Stage5::CheckRedCollision()
{
	for (auto& redBox : redBoxs) {
#pragma region 動かせるブロックと自機の当たり判定
		if (collisionOBBtoOBB(player->GetObb(), redBox->GetObb())) {
			if (!RFlag && !redBox->GetOnPlayer() && redBox->GetPosition().y == 3) {
				redBox->OnCollision(player->GetMoveF(), player->GetMoveF1(),
					player->GetMoveF2(), player->GetMoveF3());
			}
			else {
				player->FloorCollision(redBox->GetOnPlayer());
			}
		}
#pragma endregion
#pragma region 動かせるブロックと床の当たり判定
		for (auto& floor : floors) {
			if (collisionOBBtoOBB(floor->GetObb(), redBox->GetObb())) {
				if (!redonFlag) {
					redonFlag = true;
				}
				redBox->FloorCollision(redonFlag);
			}
			else {
				redonFlag = false;
			}
#pragma endregion
#pragma region 動かせるブロックと穴の当たり判定
			for (auto& cfloor : cFloor) {
				if (collisionOBBtoOBB(cfloor->GetObb(), redBox->GetObb())) {
					redBox->SetOnPlayer(true);
					if (!redBox->GetHoolFlag()) {
						redBox->SetHoolFlag(true);
					}
					redBox->FloorCollision(redBox->GetHoolFlag());
				}
			}
		}
#pragma endregion
#pragma region 動かせないブロックと旗の当たり判定
		for (auto& goal : goals) {
			if (collisionOBBtoOBB(goal->GetObb(), redBox->GetObb())) {
				player->OnCollision();
				redBox->MoveStop();
			}
		}
#pragma endregion
	}
}

void Stage5::CheckRedCCollision()
{
	for (auto& redCBox : redCBoxs) {
#pragma region 動かせないブロックと自機の当たり判定(鏡の世界Ver)
		if (collisionOBBtoOBB(playerRight->GetObb(), redCBox->GetObb())) {
			if (RFlag && !redCBox->GetOnPlayer()) {
				player->OnCollision();
				playerRight->OnCollision();
			}
			playerRight->FloorCollision(redCBox->GetOnPlayer());
		}
#pragma endregion
#pragma region 動かせないブロックと自機の当たり判定(現実世界Ver)
		if (collisionOBBtoOBB(playerRight->GetObb(), redCBox->GetObb())) {
			if (!RFlag && !redCBox->GetOnPlayer()) {
				redCBox->OnCollision(playerRight->GetMoveF(), playerRight->GetMoveF1(),
					playerRight->GetMoveF2(), playerRight->GetMoveF3());
			}
			else {
				playerRight->FloorCollision(redCBox->GetOnPlayer());
			}
		}
#pragma endregion
#pragma region 動かせないブロックと床の当たり判定
		for (auto& floor : floors) {

			if (collisionOBBtoOBB(floor->GetObb(), redCBox->GetObb())) {
				if (!redonFlag) {
					redonFlag = true;
				}
				redCBox->FloorCollision(redonFlag);
			}
			else {
				redonFlag = false;
			}
		}
#pragma endregion
#pragma region 動かせないブロックと穴の当たり判定
		for (auto& cfloor : cFloor) {
			if (collisionOBBtoOBB(cfloor->GetObb(), redCBox->GetObb())) {
				redCBox->SetOnPlayer(true);
				if (!redCBox->GetHoolFlag()) {
					redCBox->SetHoolFlag(true);
				}
				redCBox->FloorCollision(redCBox->GetHoolFlag());
			}
		}
#pragma endregion
#pragma region 動かせないブロックと旗の当たり判定
		for (auto& goal : goals) {
			if (collisionOBBtoOBB(goal->GetObb(), redCBox->GetObb())) {
				playerRight->OnCollision();
				redCBox->MoveStop();
			}
		}
#pragma endregion
	}
}

void Stage5::CheckBlueCollision()
{
#pragma region 動かせるブロックと自機の当たり判定
	for (auto& blueBox : blueBoxs) {
		if (collisionOBBtoOBB(playerRight->GetObb(), blueBox->GetObb())) {
			if (RFlag && !blueBox->GetOnPlayer() && blueBox->GetPosition().y == 3) {
				blueBox->OnCollision(playerRight->GetMoveF(), playerRight->GetMoveF1(),
					playerRight->GetMoveF2(), playerRight->GetMoveF3());
			}
			else {
				playerRight->FloorCollision(blueBox->GetOnPlayer());
			}
		}

#pragma endregion
#pragma region 動かせるブロックと床の当たり判定
		for (auto& floor : floors) {
			if (collisionOBBtoOBB(floor->GetObb(), blueBox->GetObb())) {
				if (!blueonFlag) {
					blueonFlag = true;
				}
				blueBox->FloorCollision(blueonFlag);
			}
			else {
				blueonFlag = false;
			}

		}
#pragma endregion
#pragma region 動かせるブロックと穴の当たり判定
		for (auto& cfloor : cFloor) {
			if (collisionOBBtoOBB(cfloor->GetObb(), blueBox->GetObb())) {
				blueBox->SetOnPlayer(true);
				if (!blueBox->GetHoolFlag()) {
					blueBox->SetHoolFlag(true);
				}
				blueBox->FloorCollision(blueBox->GetHoolFlag());
			}
		}
#pragma endregion
#pragma region 動かせるブロックと旗の当たり判定
		for (auto& goal : goals) {
			if (collisionOBBtoOBB(goal->GetObb(), blueBox->GetObb())) {
				playerRight->OnCollision();
				blueBox->MoveStop();
			}
		}
#pragma endregion
	}
}

void Stage5::CheckBlueCCollision()
{
	for (auto& blueCBox : blueCBoxs) {
#pragma region 動かせないブロックと自機の当たり判定(現実世界Ver)
		if (collisionOBBtoOBB(player->GetObb(), blueCBox->GetObb())) {
			if (!RFlag && !blueCBox->GetOnPlayer()) {
				player->OnCollision();
				playerRight->OnCollision();
			}
			player->FloorCollision(blueCBox->GetOnPlayer());
		}
#pragma endregion
#pragma region 動かせないブロックと自機の当たり判定(鏡の世界Ver)
		if (collisionOBBtoOBB(player->GetObb(), blueCBox->GetObb())) {
			if (RFlag && !blueCBox->GetOnPlayer()) {
				blueCBox->OnCollision(player->GetMoveF(), player->GetMoveF1(),
					player->GetMoveF2(), player->GetMoveF3());
			}
			else {
				player->FloorCollision(blueCBox->GetOnPlayer());
			}
		}
#pragma endregion
#pragma region 動かせないブロックと床の当たり判定
		for (auto& floor : floors) {
			if (collisionOBBtoOBB(floor->GetObb(), blueCBox->GetObb())) {
				if (!blueonFlag) {
					blueonFlag = true;
				}
				blueCBox->FloorCollision(blueonFlag);
			}
			else {
				blueonFlag = false;
			}
		}
#pragma endregion
#pragma region 動かせないブロックと穴の当たり判定
		for (auto& cfloor : cFloor) {
			if (collisionOBBtoOBB(cfloor->GetObb(), blueCBox->GetObb())) {
				blueCBox->SetOnPlayer(true);
				if (!blueCBox->GetHoolFlag()) {
					blueCBox->SetHoolFlag(true);
				}
				blueCBox->FloorCollision(blueCBox->GetHoolFlag());
			}
		}
#pragma endregion
#pragma region 動かせないブロックと旗の当たり判定
		for (auto& goal : goals) {
			if (collisionOBBtoOBB(goal->GetObb(), blueCBox->GetObb())) {
				player->OnCollision();
				blueCBox->MoveStop();
			}
		}
#pragma endregion
	}
}

void Stage5::BlueAndBlueCollision()
{
	for (auto& blueBox : blueBoxs) {
		for (auto& blueBox1 : blueBoxs) {
			if (blueBox->GetNumber() == blueBox1->GetNumber()) {
				continue;
			}
			if (collisionOBBtoOBB(blueBox->GetObb(), blueBox1->GetObb())) {
				playerRight->OnCollision();
				blueBox->MoveStop();
				blueBox1->MoveStop();
			}
		}
	}
	for (auto& blueCBox : blueCBoxs) {
		for (auto& blueCBox1 : blueCBoxs) {
			if (blueCBox->GetNumber() == blueCBox1->GetNumber()) {
				continue;
			}
			if (collisionOBBtoOBB(blueCBox->GetObb(), blueCBox1->GetObb())) {
				player->OnCollision();
				blueCBox->MoveStop();
				blueCBox1->MoveStop();
			}
		}
	}
}

void Stage5::RedAndRedCollision()
{
	for (auto& redBox : redBoxs) {
		for (auto& redBox1 : redBoxs) {
			if (redBox->GetNumber() == redBox1->GetNumber()) {
				continue;
			}
			if (collisionOBBtoOBB(redBox->GetObb(), redBox1->GetObb())) {
				player->OnCollision();
				redBox->MoveStop();
				redBox1->MoveStop();
			}
		}
	}
	for (auto& redCBox : redCBoxs) {
		for (auto& redCBox1 : redCBoxs) {
			if (redCBox->GetNumber() == redCBox1->GetNumber()) {
				continue;
			}
			if (collisionOBBtoOBB(redCBox->GetObb(), redCBox1->GetObb())) {
				playerRight->OnCollision();
				redCBox->MoveStop();
				redCBox1->MoveStop();
			}
		}
	}
}


void Stage5::BlueAndRedCCollision()
{
	for (auto& blueBox : blueBoxs) {
		for (auto& redCBox : redCBoxs) {
			if (collisionOBBtoOBB(blueBox->GetObb(), redCBox->GetObb())) {
				if (RFlag && (!redCBox->GetOnPlayer() || blueBox->GetOnPlayer()) && (!redCBox->GetHoolFlag() || blueBox->GetHoolFlag())) {
					if (blueBox->GetPosition().y == 3) {
						player->OnCollision();
						playerRight->OnCollision();
					}
					blueBox->MoveStop();
					redCBox->MoveStop();
				}

				if ((RFlag || !RFlag) && redCBox->GetHoolFlag()) {
					blueBox->FloorCollision(redCBox->GetHoolFlag());
				}

				if ((RFlag || !RFlag) && blueBox->GetHoolFlag()) {
					redCBox->FloorCollision(blueBox->GetHoolFlag());
				}
			}
		}
	}
}

void Stage5::RedAndBlueCCollision()
{
	for (auto& redBox : redBoxs) {
		for (auto& blueCBox : blueCBoxs) {
			if (collisionOBBtoOBB(redBox->GetObb(), blueCBox->GetObb())) {
				if (!RFlag && (!redBox->GetOnPlayer() || blueCBox->GetOnPlayer()) && (!blueCBox->GetHoolFlag() || redBox->GetHoolFlag())) {
					if (redBox->GetPosition().y == 3) {
						player->OnCollision();
						playerRight->OnCollision();
					}
					redBox->MoveStop();
					blueCBox->MoveStop();
				}
				if ((RFlag || !RFlag) && blueCBox->GetHoolFlag()) {
					redBox->FloorCollision(blueCBox->GetHoolFlag());
				}
				if ((RFlag || !RFlag) && redBox->GetHoolFlag()) {
					blueCBox->FloorCollision(redBox->GetHoolFlag());
				}
			}
		}
	}
}

void Stage5::CheckPlayerCollision()
{
#pragma region 自機と床の当たり判定
	for (auto& floor : floors) {
		if (collisionOBBtoOBB(floor->GetObb(), player->GetObb())) {
			if (!playerOnfloor) {
				playerOnfloor = true;
			}
			player->FloorCollision(playerOnfloor);
		}
		else {
			playerOnfloor = false;
		}
		if (collisionOBBtoOBB(floor->GetObb(), playerRight->GetObb())) {
			if (!playerOnfloor) {
				playerOnfloor = true;
			}
			playerRight->FloorCollision(playerOnfloor);
		}
		else {
			playerOnfloor = false;
		}
	}
#pragma endregion
	Input* input = Input::GetInstance();
	Audio* audio = Audio::GetInstance();
	for (auto& goal : goals) {
		if (collisionOBBtoOBB(goal->GetObb(), player->GetObb())) {
			if (!player->GetMoveF() && !player->GetMoveF1() && !player->GetMoveF2() && !player->GetMoveF3()) {
				goalFlag = true;
				audio->SoundStop("Mercury_Lamp.wav");
				audio->SoundPlayWave("gameclear.wav", true);
			}
			if (input->TriggerKey(DIK_SPACE) && goalFlag) {
				sceneChangeFlag = true;
			}
		}
	}
}

void Stage5::LoadPopData()
{
	//ファイルを開く
	std::ifstream file;
	file.open("Resources/csv/Stage5.csv");
	assert(file.is_open());

	//ファイル内容を文字列ストリームにコピー
	floorPopCom << file.rdbuf();

	//ファイルを閉じる
	file.close();
}

void Stage5::UpdataPopCommand()
{
	std::string line;
	//コマンド実行ループ
	while (getline(floorPopCom, line)) {
		//1行分の文字列をストリームに変換して解析しやすくする
		std::istringstream line_stream(line);

		std::string word;
		//,区切りで行の先頭文字列を取得
		getline(line_stream, word, ',');

		//"//"から始まる行はコメント
		if (word.find("//") == 0) {
			//コメント行は飛ばす
			continue;
		}

		//POPコマンド
		if (word.find("POP") == 0) {
			//x座標
			getline(line_stream, word, ',');
			float x = (float)std::atof(word.c_str());

			//y座標
			getline(line_stream, word, ',');
			float y = (float)std::atof(word.c_str());

			//z座標
			getline(line_stream, word, ',');
			float z = (float)std::atof(word.c_str());

			XMFLOAT2 point;
			getline(line_stream, word, ',');
			point.x = (float)std::atof(word.c_str());

			getline(line_stream, word, ',');
			point.y = (float)std::atof(word.c_str());

			//敵を発生させる
			//コンストラクタ呼ぶ
			std::unique_ptr<Floor> newFloor = std::make_unique<Floor>();
			newFloor->Initialize(XMFLOAT3(x, y, z), XMFLOAT2(point));
			//障害物を登録する
			floors.push_back(std::move(newFloor));
		}
		else if (word.find("CPOP") == 0) {
			//x座標
			getline(line_stream, word, ',');
			float x = (float)std::atof(word.c_str());

			//y座標
			getline(line_stream, word, ',');
			float y = (float)std::atof(word.c_str());

			//z座標
			getline(line_stream, word, ',');
			float z = (float)std::atof(word.c_str());

			XMFLOAT2 point;
			getline(line_stream, word, ',');
			point.x = (float)std::atof(word.c_str());

			getline(line_stream, word, ',');
			point.y = (float)std::atof(word.c_str());

			//コンストラクタ呼ぶ
			std::unique_ptr<ClearBox> newClearBox = std::make_unique<ClearBox>();
			newClearBox->Initialize(XMFLOAT3(x, y, z), XMFLOAT2(point));
			//障害物を登録する
			clearBox.push_back(std::move(newClearBox));
		}
		else if (word.find("CFPOP") == 0) {
			//x座標
			getline(line_stream, word, ',');
			float x = (float)std::atof(word.c_str());

			//y座標
			getline(line_stream, word, ',');
			float y = (float)std::atof(word.c_str());

			//z座標
			getline(line_stream, word, ',');
			float z = (float)std::atof(word.c_str());

			XMFLOAT2 point;
			getline(line_stream, word, ',');
			point.x = (float)std::atof(word.c_str());

			getline(line_stream, word, ',');
			point.y = (float)std::atof(word.c_str());

			//コンストラクタ呼ぶ
			std::unique_ptr<CFloor> newCFloor = std::make_unique<CFloor>();
			newCFloor->Initialize(XMFLOAT3(x, y, z), XMFLOAT2(point));
			//障害物を登録する
			cFloor.push_back(std::move(newCFloor));
		}
		else if (word.find("RPOP") == 0) {
			//x座標
			getline(line_stream, word, ',');
			float x = (float)std::atof(word.c_str());

			//y座標
			getline(line_stream, word, ',');
			float y = (float)std::atof(word.c_str());

			//z座標
			getline(line_stream, word, ',');
			float z = (float)std::atof(word.c_str());

			XMFLOAT2 point;
			getline(line_stream, word, ',');
			point.x = (float)std::atof(word.c_str());

			getline(line_stream, word, ',');
			point.y = (float)std::atof(word.c_str());

			//コンストラクタ呼ぶ
			std::unique_ptr<RedBox> newRedBox = std::make_unique<RedBox>();
			newRedBox->Initialize(XMFLOAT3(x, y, z), XMFLOAT2(point));
			//障害物を登録する
			redBoxs.push_back(std::move(newRedBox));
		}
		else if (word.find("BPOP") == 0) {
			//x座標
			getline(line_stream, word, ',');
			float x = (float)std::atof(word.c_str());

			//y座標
			getline(line_stream, word, ',');
			float y = (float)std::atof(word.c_str());

			//z座標
			getline(line_stream, word, ',');
			float z = (float)std::atof(word.c_str());

			XMFLOAT2 point;
			getline(line_stream, word, ',');
			point.x = (float)std::atof(word.c_str());

			getline(line_stream, word, ',');
			point.y = (float)std::atof(word.c_str());

			//コンストラクタ呼ぶ
			std::unique_ptr<BlueBox> newBlueBox = std::make_unique<BlueBox>();
			newBlueBox->Initialize(XMFLOAT3(x, y, z), XMFLOAT2(point));
			//障害物を登録する
			blueBoxs.push_back(std::move(newBlueBox));
		}
		else if (word.find("RCPOP") == 0) {
			//x座標
			getline(line_stream, word, ',');
			float x = (float)std::atof(word.c_str());

			//y座標
			getline(line_stream, word, ',');
			float y = (float)std::atof(word.c_str());

			//z座標
			getline(line_stream, word, ',');
			float z = (float)std::atof(word.c_str());

			XMFLOAT2 point;
			getline(line_stream, word, ',');
			point.x = (float)std::atof(word.c_str());

			getline(line_stream, word, ',');
			point.y = (float)std::atof(word.c_str());

			//コンストラクタ呼ぶ
			std::unique_ptr<RedCBox> newRedCBox = std::make_unique<RedCBox>();
			newRedCBox->Initialize(XMFLOAT3(x, y, z), XMFLOAT2(point));
			//障害物を登録する
			redCBoxs.push_back(std::move(newRedCBox));
		}
		else if (word.find("BCPOP") == 0) {
			//x座標
			getline(line_stream, word, ',');
			float x = (float)std::atof(word.c_str());

			//y座標
			getline(line_stream, word, ',');
			float y = (float)std::atof(word.c_str());

			//z座標
			getline(line_stream, word, ',');
			float z = (float)std::atof(word.c_str());

			XMFLOAT2 point;
			getline(line_stream, word, ',');
			point.x = (float)std::atof(word.c_str());

			getline(line_stream, word, ',');
			point.y = (float)std::atof(word.c_str());

			//コンストラクタ呼ぶ
			std::unique_ptr<BlueCBox> newBlueCBox = std::make_unique<BlueCBox>();
			newBlueCBox->Initialize(XMFLOAT3(x, y, z), XMFLOAT2(point));
			//障害物を登録する
			blueCBoxs.push_back(std::move(newBlueCBox));
		}
		else if (word.find("GPOP") == 0) {
			//x座標
			getline(line_stream, word, ',');
			float x = (float)std::atof(word.c_str());

			//y座標
			getline(line_stream, word, ',');
			float y = (float)std::atof(word.c_str());

			//z座標
			getline(line_stream, word, ',');
			float z = (float)std::atof(word.c_str());

			//x軸
			getline(line_stream, word, ',');
			float rx = (float)std::atof(word.c_str());

			//y軸
			getline(line_stream, word, ',');
			float ry = (float)std::atof(word.c_str());

			//z軸
			getline(line_stream, word, ',');
			float rz = (float)std::atof(word.c_str());

			XMFLOAT2 point;
			getline(line_stream, word, ',');
			point.x = (float)std::atof(word.c_str());

			getline(line_stream, word, ',');
			point.y = (float)std::atof(word.c_str());

			//コンストラクタ呼ぶ
			std::unique_ptr<Goal> newGoal = std::make_unique<Goal>();
			newGoal->Initialize(XMFLOAT3(x, y, z), XMFLOAT3(rx, ry, rz), XMFLOAT2(point));
			//障害物を登録する
			goals.push_back(std::move(newGoal));
		}
	}
}