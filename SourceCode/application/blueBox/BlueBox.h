#pragma once
#include "GameObject.h"
#include "Fbx_Object3d.h"
#include "DirectXCommon.h"
#include "Object3d.h"
#include "TouchableObject.h"
#include "Shake.h"
#include "Audio.h"
#include "OBB.h"
#include "PlayerRight.h"
#include <DirectXMath.h>
#include <memory>

using namespace DirectX;

class BlueBox : public Object3d
{
public:
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;

public:
	~BlueBox();

	void Initialize(XMFLOAT3 pos, XMFLOAT2 point);

	void Update();

	void Draw();

	void OnCollision(bool moveF = false, bool moveF1 = false, bool moveF2 = false, bool moveF3 = false);
	void MoveStop();
	void FloorCollision(bool onFlag = false);
	inline OBB GetObb() { return obb; }
	inline bool GetHoolFlag() const { return hoolFlag; }
	inline void SetHoolFlag(bool hoolFlag) { this->hoolFlag = hoolFlag; }
	inline bool GetOnPlayer() const { return onPlayer; }
	inline void SetOnPlayer(bool onPlayer) { this->onPlayer = onPlayer; }

	inline int GetNumber() { return number; }

	inline const XMFLOAT3& GetPosition() { return position; }
protected:
	XMFLOAT3 position;
	XMFLOAT3 rotation;
	std::unique_ptr<Object3d> object;
	bool flag = false;
	bool hoolFlag = false;
	bool onPlayer = false;
	float speed = 0.3f;
	float moveS = 0.15f;
	XMFLOAT4 color = { 1.0f, 1.0f, 1.0f, 1.0f };
	//イージング用
	float easFrame = 0.0f;
	float downFrame = 0.0f;
	//シェイク用
	std::unique_ptr<Shake> shake = nullptr;
	bool shakeF = false;
	XMFLOAT3 shakePos = { 0.0f,0.0f,0.0f };
	Audio* audio = nullptr;
	OBB obb = {};
	PlayerRight* playerR = nullptr;

	int number = 0;
};

