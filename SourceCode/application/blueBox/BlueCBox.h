#pragma once
#include "GameObject.h"
#include "Fbx_Object3d.h"
#include "DirectXCommon.h"
#include "Object3d.h"
#include "TouchableObject.h"
#include "Shake.h"
#include "Audio.h"
#include "OBB.h"
#include <DirectXMath.h>
#include <memory>

using namespace DirectX;

class BlueCBox : public Object3d
{
public:
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;

public:
	~BlueCBox();

	bool Initialize(XMFLOAT3 pos, XMFLOAT2 point);

	void Update();

	void Draw();

	void OnCollision(bool moveF, bool moveF1, bool moveF2, bool moveF3);
	void MoveStop();
	void FloorCollision(bool onFlag = false);
public:
	inline OBB GetObb() { return obb; }
	inline bool GetHoolFlag() const { return hoolFlag; }
	inline void SetHoolFlag(bool hoolFlag) { this->hoolFlag = hoolFlag; }
	inline bool GetOnPlayer() const { return onPlayer; }
	inline void SetOnPlayer(bool onPlayer) { this->onPlayer = onPlayer; }

	inline int GetNumber() { return number; }

protected:
	//XMFLOAT3 position;
	std::unique_ptr<Object3d> object;
	bool flag = false;
	bool hoolFlag = false;
	bool onPlayer = false;
	float speed = 0.3f;
	float moveS = 0.15f;
	XMFLOAT4 color = { 1.0f, 1.0f, 1.0f, 1.0f };
	//イージング用
	float easFrame = 0.0f;
	float downFrame = 0.0f;
	//シェイク用
	std::unique_ptr<Shake> shake = nullptr;
	bool shakeF = false;
	XMFLOAT3 shakePos = { 0.0f,0.0f,0.0f };
	Audio* audio = nullptr;
	OBB obb = {};

	int number = 0;
};
