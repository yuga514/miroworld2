#include "KeyHoleR.h"
#include "Easing.h"

KeyHoleR::~KeyHoleR()
{
}

void KeyHoleR::Initialize(XMFLOAT3 pos)
{
	object = Object3d::Create();
	object->SetModel(Model::CreateFromOBJ("lock_left"));
	object->SetScale({ 1.5f, 1.5f, 1.5f });
	object->SetRotation({ 0,90,0 });
	position = pos;
	obb.Create();
}

void KeyHoleR::Update()
{
	object->SetPosition(position);
	obb.SetVector(0, XMVECTOR{ 1, 0, 0, 0 });
	obb.SetVector(1, XMVECTOR{ 0, 1, 0, 0 });
	obb.SetVector(2, XMVECTOR{ 0, 0, 1, 0 });
	obb.SetPos(position);
	obb.SetLength(0, 0.73f);
	obb.SetLength(1, 1.46f);
	obb.SetLength(2, 1.46f);

	if (flag) {
		timer += 1.0f / 1200.f;
	}
	if (timer <= 0.0365f) {
		position.x = Ease(Out, Quad, timer, position.x, position.x + 1.5f);
	}
	if (timer >= 0.0365f) {
		object->SetRotation(XMFLOAT3(0, 90, r));
		r += 2.f;
		object->SetScale(XMFLOAT3(q, w, e));
		q -= 0.015f;
		w -= 0.015f;
		e -= 0.015f;
		if (q == w == e <= 0.f) {
			q = w = e = 0;
		}
	}
	if (timer >= 30) {
		flag = false;
	}

	object->Update();
}

void KeyHoleR::Draw()
{
	if (timer <= 30) {
		object->Draw();
	}
}

void KeyHoleR::OnCollision()
{
	flag = true;
}