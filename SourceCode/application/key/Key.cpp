#include "Key.h"
#include "Easing.h"
#include "Audio.h"

Key::~Key()
{
}

void Key::Initialize(XMFLOAT3 pos, XMFLOAT3 rot)
{
	object = Object3d::Create();
	object->SetModel(Model::CreateFromOBJ("key"));
	object->SetScale({ 1.5f, 1.5f, 1.5f });
	position = pos;
	rotation = rot;
	obb.Create();
}

void Key::Update()
{
	if (flag) {
		if (easFrame < 1.0f) {
			easFrame += 0.01f;
		}
		if (position.y <= 6.2f) {
			position.y = Ease(Out, Quad, easFrame, position.y, 6.2f);
		}
		if (position.y >= 6.f) {
			flag = false;
		}
	}

	object->SetPosition(position);
	object->SetRotation(rotation);
	obb.SetVector(0, XMVECTOR{ 1, 0, 0, 0 });
	obb.SetVector(1, XMVECTOR{ 0, 1, 0, 0 });
	obb.SetVector(2, XMVECTOR{ 0, 0, 1, 0 });
	obb.SetPos(position);
	obb.SetLength(0, 1.46f);
	obb.SetLength(1, 1.46f);
	obb.SetLength(2, 1.46f);
	object->Update();
}

void Key::Draw()
{
	if (position.y <= 6.f) {
		object->Draw();
	}
}

void Key::OnCollision()
{
	flag = true;
}