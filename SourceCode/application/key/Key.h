#pragma once
#include "Object3d.h"
#include "OBB.h"
#include <DirectXMath.h>
#include <memory>

using namespace DirectX;

class Key : public Object3d
{
public:
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;

public:
	~Key();

	void Initialize(XMFLOAT3 pos, XMFLOAT3 rot);

	void Update();

	void Draw();

	void OnCollision();
	inline OBB GetObb() { return obb; }
	inline bool GetFlag() const { return flag; }
	inline void SetFlag(bool flag) { this->flag = flag; }
	inline const XMFLOAT3& GetPosition() { return position; }
protected:
	XMFLOAT3 position;
	XMFLOAT3 rotation;
	std::unique_ptr<Object3d> object;
	bool flag = false;
	XMFLOAT4 color = { 1.0f, 1.0f, 1.0f, 1.0f };
	//イージング用
	float easFrame = 0.0f;
	float downFrame = 0.0f;
	OBB obb = {};
};

