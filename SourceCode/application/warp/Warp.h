#pragma once
#include "Object3d.h"
#include "GameObject.h"
#include "Fbx_Object3d.h"
#include "DirectXCommon.h"
#include "Object3d.h"
#include "TouchableObject.h"
#include "Shake.h"
#include "Audio.h"
#include "OBB.h"
#include "Player.h"
#include "PlayerRight.h"
#include <DirectXMath.h>
#include <memory>

using namespace DirectX;

class Warp : public Object3d
{
public:
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
public:
	Warp();
	~Warp();

	bool Initialize();
	bool Initialize2();
	bool Initialize3();
	bool Initialize4();
	bool Initialize5();
	bool Initialize6();
	void Update();
	void Update2();
	void Draw();
	void Draw2();
	//inline OBB GetObb() { return obb[8]; }
private:
	std::unique_ptr<Object3d> object[8];
	XMFLOAT3 position[8];
	XMFLOAT3 rotation[8];
	OBB obb[8] = {};

	std::unique_ptr<Object3d> object2[4];
	XMFLOAT3 position2[4];
	XMFLOAT3 rotation2[4];
	OBB obb2[4] = {};
	XMFLOAT4 color = {1.0f, 1.0f, 1.0f, 1.0f};
};