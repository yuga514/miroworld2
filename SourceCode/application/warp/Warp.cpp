#include "Warp.h"
#include "OBBCollision.h"

Warp::Warp()
{
}

Warp::~Warp()
{
}

bool Warp::Initialize()
{
	for (int i = 0; i < 8; i++) {
		obb[i].Create();
		object[i] = Object3d::Create();
		object[i]->SetModel(Model::CreateFromOBJ("warp"));
		object[i]->SetScale({ 1.5f, 1.5f, 1.5f });

		position[i] = object[i]->GetPosition();
		//Left
		position[0] = { -37,3,12 };
		position[1] = { -43,3,21 };
		position[2] = { -31,3,12 };
		position[3] = { -25,3,18 };
		//Right
		position[4] = { 37,3,12 };
		position[5] = { 43,3,21 };
		position[6] = { 31,3,12 };
		position[7] = { 25,3,18 };
		object[i]->SetPosition(position[i]);
	}
	return true;
}

bool Warp::Initialize2()
{
	for (int i = 0; i < 4; i++) {
		obb2[i].Create();
		object2[i] = Object3d::Create();
		object2[i]->SetModel(Model::CreateFromOBJ("warp"));
		object2[i]->SetScale({ 1.5f, 1.5f, 1.5f });

		position[i] = object2[i]->GetPosition();
		//Left
		position2[0] = { -69,3,12 };
		position2[1] = { -87,3,12 };
		position2[2] = { 69,3,12 };
		position2[3] = { 87,3,12 };
		object2[i]->SetPosition(position2[i]);
	}
	return true;
}

bool Warp::Initialize3()
{
	for (int i = 0; i < 8; i++) {
		obb[i].Create();
		object[i] = Object3d::Create();
		object[i]->SetModel(Model::CreateFromOBJ("warp"));
		object[i]->SetScale({ 1.5f, 1.5f, 1.5f });

		position[i] = object[i]->GetPosition();
		//Left
		position[0] = { -87,3,6 };
		position[1] = { -93,3,33 };
		position[2] = { -69,3,9 };
		position[3] = { -63,3,9 };
		//Right
		position[4] = { 87,3,6 };
		position[5] = { 93,3,33 };
		position[6] = { 69,3,9 };
		position[7] = { 63,3,9 };
		object[i]->SetPosition(position[i]);
	}
	return true;
}

bool Warp::Initialize4()
{
	for (int i = 0; i < 4; i++) {
		obb2[i].Create();
		object2[i] = Object3d::Create();
		object2[i]->SetModel(Model::CreateFromOBJ("warp"));
		object2[i]->SetScale({ 1.5f, 1.5f, 1.5f });

		position[i] = object2[i]->GetPosition();
		position2[0] = { -49,3,0 };
		position2[1] = { -34,3,3 };
		position2[2] = { 49,3,0 };
		position2[3] = { 34,3,3 };
		object2[i]->SetPosition(position2[i]);
	}
	return true;
}

bool Warp::Initialize5()
{
	for (int i = 0; i < 4; i++) {
		obb2[i].Create();
		object2[i] = Object3d::Create();
		object2[i]->SetModel(Model::CreateFromOBJ("warp"));
		object2[i]->SetScale({ 1.5f, 1.5f, 1.5f });

		position[i] = object2[i]->GetPosition();
		position2[0] = { -60,3,21 };
		position2[1] = { -69,3,3 };
		position2[2] = { 60,3,21 };
		position2[3] = { 69,3,3 };
		object2[i]->SetPosition(position2[i]);
	}
	return true;
}

bool Warp::Initialize6()
{
	for (int i = 0; i < 4; i++) {
		obb2[i].Create();
		object2[i] = Object3d::Create();
		object2[i]->SetModel(Model::CreateFromOBJ("warp"));
		object2[i]->SetScale({ 1.5f, 1.5f, 1.5f });

		position[i] = object2[i]->GetPosition();
		position2[0] = { -40,3,21 };
		position2[1] = { -40,3,0 };
		position2[2] = { 40,3,21 };
		position2[3] = { 40,3,0 };
		object2[i]->SetPosition(position2[i]);
	}
	return true;
}

void Warp::Update()
{

	for (int i = 0; i < 8; i++) {
		obb[i].SetVector(0, XMVECTOR{ 1, 0, 0, 0 });
		obb[i].SetVector(1, XMVECTOR{ 0, 1, 0, 0 });
		obb[i].SetVector(2, XMVECTOR{ 0, 0, 1, 0 });
		obb[i].SetPos(position[i]);
		obb[i].SetLength(0, 1.46f);
		obb[i].SetLength(1, 3.3f);
		obb[i].SetLength(2, 1.46f);
		object[i]->Update();
		object[i]->SetPosition(position[i]);
	}
}

void Warp::Update2()
{
	for (int i = 0; i < 4; i++) {
		obb2[i].SetVector(0, XMVECTOR{ 1, 0, 0, 0 });
		obb2[i].SetVector(1, XMVECTOR{ 0, 1, 0, 0 });
		obb2[i].SetVector(2, XMVECTOR{ 0, 0, 1, 0 });
		obb2[i].SetPos(position2[i]);
		obb2[i].SetLength(0, 1.46f);
		obb2[i].SetLength(1, 3.3f);
		obb2[i].SetLength(2, 1.46f);
		object2[i]->Update();
		object2[i]->SetPosition(position2[i]);
	}
}

void Warp::Draw()
{
	for (int i = 0; i < 8; i++) {
		object[i]->Draw();
	}
}

void Warp::Draw2()
{
	for (int i = 0; i < 4; i++) {
		object2[i]->Draw();
	}
}
