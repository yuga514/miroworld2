#include "Goal.h"
#include "Obb.h"
Goal::~Goal()
{
}

bool Goal::Initialize(XMFLOAT3 pos, XMFLOAT3 rotate,XMFLOAT2 point)
{
	object = Object3d::Create();
	object->SetModel(Model::CreateFromOBJ("GoalFlag"));
	object->SetScale({ 1.6f, 1.6f, 1.6f });
	position = pos;
	rotation = rotate;
	obb.Create();
	return true;
}

void Goal::Update()
{
	obb.SetVector(0, XMVECTOR{ 1, 0, 0, 0 });
	obb.SetVector(1, XMVECTOR{ 0, 1, 0, 0 });
	obb.SetVector(2, XMVECTOR{ 0, 0, 1, 0 });
	obb.SetPos(position);
	obb.SetLength(0, 1.46f);
	obb.SetLength(1, 3.3f);
	obb.SetLength(2, 1.46f);
	object->Update();
	object->SetPosition(position);
	object->SetRotation(rotation);
}

void Goal::Draw()
{
	object->Draw();
}

void Goal::OnCollision(bool flag)
{
}
