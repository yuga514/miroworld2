#pragma once
#include "Object3d.h"
#include "OBB.h"

using namespace DirectX;

class Goal : public Object3d
{
public:
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;

public:
	~Goal();

	bool Initialize(XMFLOAT3 pos, XMFLOAT3 rotate,XMFLOAT2 point);

	void Update();

	void Draw();

	void OnCollision(bool flag = false);
	inline OBB GetObb() { return obb; }
	inline const XMFLOAT3& GetPosition() { return position; }
protected:
	XMFLOAT3 position;
	XMFLOAT3 pos;
	XMFLOAT3 rotation;
	std::unique_ptr<Object3d> object;
	bool alive = true;
	XMFLOAT4 color = { 1.0f, 1.0f, 1.0f, 1.0f };
	//イージング用
	float easFrame = 0.0f;
	float downFrame = 0.0f;
	//シェイク用
	//std::unique_ptr<Shake> shake = nullptr;
	bool shakeF = false;
	XMFLOAT3 shakePos = { 0.0f,0.0f,0.0f };
	//Audio* audio = nullptr;
	OBB obb = {};
};