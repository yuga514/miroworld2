﻿#include "Player.h"
#include "Input.h"
#include "ParticleManager.h"
#include "SphereCollider.h"
#include "CollisionManager.h"
#include "CollisionAttribute.h"
#include "Collision.h"
#include "OBB.h"
Player* Player::Create(Model* model)
{
	//3D�I�u�W�F�N�g�̃C���X�^���X�𐶐�
	Player* instance = new Player();
	if (instance == nullptr) {
		return nullptr;
	}

	//������
	if (!instance->Initialize()) {
		delete instance;
		assert(0);
	}
	//���f���̃Z�b�g
	if (model) {
		instance->SetModel(model);
	}

	return instance;
}

Player::~Player()
{
}

bool Player::Initialize()
{
	if (!Object3d::Initialize()) {
		return false;
	}
	particleMan = ParticleManager::GetInstance();

	Object3d::SetRotation({ 0,0,0 });
	Object3d::SetScale({ 1.5f,1.5f,1.5f });
	camera.reset(new FollowingCamera());
	shake.reset(new Shake());
	obb.Create();
	AudioLoad();
	return true;
}

void Player::AudioLoad()
{
}

void Player::Update()
{
	Audio* audio = Audio::GetInstance();
	move();
	Input* input = Input::GetInstance();
	Object3d::SetPosition(position);
	obb.SetPos(position);
	obb.SetVector(0, XMVECTOR{ 1, 0, 0, 0 });
	obb.SetVector(1, XMVECTOR{ 0, 1, 0, 0 });
	obb.SetVector(2, XMVECTOR{ 0, 0, 1, 0 });
	obb.SetLength(0, 1.46f);
	obb.SetLength(1, 3.f);
	obb.SetLength(2, 1.46f);
	Object3d::Update();
}

//�����������Ȃ����p�̃A�b�v�f�[�g
void Player::StopUpdate()
{
	Object3d::Update();
}

//�ړ�
void Player::move()
{
	Input* input = Input::GetInstance();
	rotation = Object3d::GetRotation();
	position = Object3d::GetPosition();

	position.y -= downSpeed;

	if (!moveF && !moveF1 && !moveF2 && !moveF3) {
		moveSpeed = 0.15f;
	}

	if (rotation.x == 0 && rotation.z == 0 && position.y == 3) {
		if (input->TriggerKey(DIK_W)) {
			moveF2 = true;
		}
		else if (input->TriggerKey(DIK_S)) {
			moveF3 = true;
		}
		else if (input->TriggerKey(DIK_D)) {
			moveF = true;
		}
		else if (input->TriggerKey(DIK_A)) {
			moveF1 = true;
		}
	}

	if (moveF2) {
		position.z += moveSpeed;
		rotation.x += rotate;
	}
	if (moveF3) {
		position.z -= moveSpeed;
		rotation.x -= rotate;
	}
	if (moveF) {
		position.x += moveSpeed;
		rotation.z -= rotate;
	}
	if (moveF1) {
		position.x -= moveSpeed;
		rotation.z += rotate;
	}

	if ((int)rotation.x % 90 == 0 && rotation.x != 0 || (int)rotation.z % 90 == 0 && rotation.z != 0) {
		moveF = false;
		moveF1 = false;
		moveF2 = false;
		moveF3 = false;
		downSpeed = 0.3f;
		Object3d::SetRotation({ 0,0,0 });
	}
	if (spaceF == true) {
		warpFlag = false;
	}
	else {
		warpFlag = true;
	}
	// ���W�̕ύX�𔽉f
	Object3d::SetPosition(position);
}

//�X�P�[��������������p�֐�
void Player::ScaleSmall()
{
	scale = Object3d::GetScale();
	if ((scale.x >= 0 && scale.y >= 0 && scale.z >= 0)) {
		scale.x -= 0.03f;
		scale.y -= 0.03f;
		scale.z -= 0.03f;
		Object3d::SetScale(scale);
	}
}

//�X�P�[����傫������p�֐�
void Player::ScaleLarge()
{
	scale = Object3d::GetScale();
	if (scale.x <= 1.5f && scale.y <= 1.5f && scale.z <= 1.5f) {
		scale.x += 0.03f;
		scale.y += 0.03f;
		scale.z += 0.03f;
		Object3d::SetScale(scale);
	}
}

void Player::OnCollision()
{
	moveSpeed = 0.f;
	position.x = round(position.x);
	position.z = round(position.z);
}

void Player::FloorCollision(bool onFlag)
{
	if (onFlag) {
		downSpeed = 0.f;
		onGround = true;
		if (position.y < 3) {
			position.y = 3.f;
		}
	}
	if (!onFlag) {
		onGround = false;
	}
}

//�p�[�e�B�N���̐���
void Player::CreateParticle()
{
	for (int j = 0; j < 100; j++) {
		DirectX::XMFLOAT3 pos = Object3d::GetPosition();
		pos.y = 5;
		//X,Y,Z�S��[-0.05f, +0.05f]�Ń����_���ɕ��z
		const float md_vel = 0.20f;
		DirectX::XMFLOAT3 vel{};
		vel.x = (float)rand() / RAND_MAX * md_vel - md_vel / 2.0f;
		vel.y = (float)rand() / RAND_MAX * md_vel - md_vel / 2.0f;
		vel.z = (float)rand() / RAND_MAX * md_vel - md_vel / 2.0f;
		//�d�͂Ɍ����Ă�Y�̂�[-0.001f, 0]�Ń����_���ɕ��z
		DirectX::XMFLOAT3 acc{};
		const float rnd_acc = 0.005f;
		acc.y = -(float)rand() / RAND_MAX * rnd_acc;
		//�ǉ�
		particleMan->Add(60, pos, vel, acc, 1.0f, 0.0f);

	}
}

void Player::Draw()
{
	Object3d::Draw();
}

XMVECTOR Player::GetWorldPosition()
{
	XMVECTOR worldPos{};

	worldPos.m128_f32[0] = position.x;
	worldPos.m128_f32[1] = position.y;
	worldPos.m128_f32[2] = position.z;

	return worldPos;
}