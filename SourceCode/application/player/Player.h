#pragma once
#include "Object3d.h"
#include "GameObject.h"
#include "Shake.h"
#include "Input.h"
#include "FollowingCamera.h"
#include "Framework.h"
#include "Audio.h"
#include "OBB.h"
#include <memory>
#include <list>
#include <forward_list>
#include <DirectXMath.h>
class ParticleManager;

using namespace DirectX;

class Player : public Object3d
{
public:
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;
	using XMVECTOR = DirectX::XMVECTOR;
public:
	/// <summary>
	/// 3Dオブジェクト生成
	/// </summary>
	/// <returns>インスタンス</returns>
	static Player* Create(Model* model = nullptr);
public:
	//コンストラクタ
	//Player();

	~Player();

	bool Initialize() override;

	void AudioLoad();

	//更新
	void Update() override;

	//動かしたくない時用更新
	void StopUpdate();

	//移動
	void move();

	//スケールを小さくする
	void ScaleSmall();

	void ScaleLarge();

	//描画
	void Draw();

	//当たり判定
	void OnCollision();

	//当たり判定
	void FloorCollision(bool onFlag = false);

	//void Warp

	//パーティク生成
	void CreateParticle();
	//ワールド座標を取得
	XMVECTOR GetWorldPosition();

	inline bool GetAlive() const { return alive; }
	inline void SetAlive(bool alive) { this->alive = alive; }

	inline bool GetMoveF() const { return moveF; }
	inline bool GetMoveF1() const { return moveF1; }
	inline bool GetMoveF2() const { return moveF2; }
	inline bool GetMoveF3() const { return moveF3; }

	inline OBB GetObb() { return obb; }
private:
	std::unique_ptr<Object3d> object;
	ParticleManager* particleMan = nullptr;
	bool alive = true;
	bool onGround = false;
	bool moveF = false;
	bool moveF1 = false;
	bool moveF2 = false;
	bool moveF3 = false;
	bool spaceF = false;
	// ワープ準備
	bool warpFlag = false;
	float moveSpeed = 0.15f;
	float downSpeed = 0.3f;
	float rotate = 4.5f;
	//シェイク用
	std::unique_ptr<Shake> shake = nullptr;
	OBB obb = {};
	XMFLOAT3 oldPosition;
	bool shakeF = false;
	XMFLOAT3 shakePos = { 0.0f,0.0f,0.0f };
	//音
	Audio* audio = nullptr;
	std::unique_ptr<FollowingCamera> camera;
};

