#include "ClearBox.h"

ClearBox::~ClearBox()
{
}

void ClearBox::Initialize(XMFLOAT3 pos ,XMFLOAT2 point)
{
	object = Object3d::Create();
	object->SetModel(Model::CreateFromOBJ("ClearBox"));
	object->SetScale({ 1.4f, 1.2f, 1.4f });
	object->SetPosition(pos);
}

void ClearBox::Update()
{
	object->Update();
}

void ClearBox::Draw()
{
	object->Draw();
}

void ClearBox::OnCollision(bool flag)
{
}
