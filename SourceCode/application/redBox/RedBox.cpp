#include "RedBox.h"

RedBox::~RedBox()
{
}

void RedBox::Initialize(XMFLOAT3 pos,XMFLOAT2 point)
{
	object = Object3d::Create();
	object->SetModel(Model::CreateFromOBJ("PlayerRed"));
	object->SetScale({ 1.5f, 1.5f, 1.5f });
	object->SetRotation({ 0,180,0 });
	position = pos;
	obb.Create();
	player->Create(Model::CreateFromOBJ("RedBox"));

	number = rand() % RAND_MAX;
}

void RedBox::Update()
{
	obb.SetVector(0, XMVECTOR{ 1, 0, 0, 0 });
	obb.SetVector(1, XMVECTOR{ 0, 1, 0, 0 });
	obb.SetVector(2, XMVECTOR{ 0, 0, 1, 0 });
	obb.SetPos(position);
	obb.SetLength(0, 1.46f);
	obb.SetLength(1, 3.3f);
	obb.SetLength(2, 1.46f);
	position.y -= speed;
	speed = 0.3f;
	if (position.y < 3) {
		position.x = round(position.x);
		position.z = round(position.z);
	}
	object->Update();
	object->SetPosition(position);
}

void RedBox::Draw()
{
	object->Draw();
}

void RedBox::OnCollision(bool moveF, bool moveF1, bool moveF2, bool moveF3)
{
	if (moveF) {
		position.x += moveS;
	}
	if (moveF1) {
		position.x -= moveS;
	}
	if (moveF2) {
		position.z += moveS;
	}
	if (moveF3) {
		position.z -= moveS;
	}
	if (!moveF && !moveF1 && !moveF2 && !moveF3) {
		position.x = round(position.x);
		position.z = round(position.z);
	}
}

void RedBox::MoveStop()
{
	position.x = round(position.x);
	position.z = round(position.z);
}

void RedBox::FloorCollision(bool onFlag)
{
	if (onFlag) {
		speed = 0.f;
		flag = true;
		position.y = round(position.y);
	}
	if (!onFlag) {
		flag = false;
	}
}
